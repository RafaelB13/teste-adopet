import ContainerLista from '../Utils/ContainerListagem'

export function FiltraUsuarioPeloEmail(users, valueFiltro){ 
    const containerCards = document.querySelector('.container-cards')   
    containerCards.innerHTML = ''    

    const usuarioFiltrado = users.filter(user => {
        return user.email.indexOf(valueFiltro) > -1
    })    

    usuarioFiltrado.map(user => {
        containerCards.innerHTML += ContainerLista(user)        
    })  
}