const ContainerLista = (user) => `<div class="card">
    <div class="card-title">
    <h3><strong>${user.name}</strong></h3>
    <p>Usuário: <strong>${user.username}</strong></p>
    <p>Email: <strong>${user.email}</strong></p>
    <p>Telefone: <strong>${user.phone}</strong></p>
    <p>Website: <strong>${user.website}</strong></p>
    <p>Endereço: <strong>${user.address.street + ", " 
        + user.address.suite +", "
        + user.address.city + ", "
        + user.address.zipcode }</strong>
    </p>   
    
    <div class="card-body">
    
    </div>
</div>
`

export default ContainerLista