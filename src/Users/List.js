import ContainerLista from '../Utils/ContainerListagem'

export function Lista(users){    
    const containerCards = document.querySelector('.container-cards')
    users.map(user => {        
        containerCards.innerHTML += ContainerLista(user)
    })    
}