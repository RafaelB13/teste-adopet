import api from './api'

async function getUsers(): Promise<any> {
    const url = '/users'
            
    const response = await api.get(url)
    
    return response
}

export {
    getUsers
}
