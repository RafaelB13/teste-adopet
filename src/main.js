import {getUsers} from './service'
import {Lista} from './Users/List'
import {FiltraUsuarioPeloEmail} from './Utils/Filtro'
async function main(){        
    const response = await getUsers()
    const loader = document.querySelector('.loader')
    const app = document.querySelector('.app')
    const btnFiltro = document.querySelector('.btn-filtro')        

    if(response.status !== 200)
        return alert('Deu merda no requets')

    const users = response.data
    Lista(users)

    setTimeout(() => {
        loader.classList.add('disable')
        app.classList.add('enable')
    }, 1000)

    btnFiltro.addEventListener('click', e => {
        const valueFiltro = document.getElementById("valueFiltro").value
        e.preventDefault()
        FiltraUsuarioPeloEmail(users, valueFiltro)
    })
}

main()