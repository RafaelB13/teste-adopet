const path = require('path');

module.exports = {
    entry: './src/main.js',
    devtool: 'inline-source-map',     
    module: {
    rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,
        },
    ],
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
    },
    output: {
        publicPath: '/',
        path: __dirname + '/public',
        filename: 'bundle-.js',
    },
    devServer: {
        contentBase: __dirname + '/public'
    }
};